package hr.josipva.githubviewer.ui.components.details

import androidx.lifecycle.SavedStateHandle
import hr.josipva.githubviewer.repository.FakeGithubRepository
import hr.josipva.githubviewer.ui.Routes
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class DetailsViewModelTest {

    private lateinit var detailsViewModel: DetailsViewModel

    @Before
    fun setUp() {
        val savedStateHandle = SavedStateHandle()
        savedStateHandle[Routes.Details.loginArg] = "jvuletica"
        savedStateHandle[Routes.Details.repoNameArg] = "Chakula"

        detailsViewModel = DetailsViewModel(
            dispatcher = TestCoroutineDispatcher(),
            savedStateHandle = savedStateHandle,
            githubRepository = FakeGithubRepository()
        )
    }

    @Test
    fun initializedVMHasUser() {
        assertNotNull(detailsViewModel.state.user)
    }

    @Test
    fun initializedVMHasGithubRepository() {
        assertNotNull(detailsViewModel.state.repository)
    }
}