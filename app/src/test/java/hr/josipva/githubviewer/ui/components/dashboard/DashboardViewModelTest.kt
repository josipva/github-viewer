package hr.josipva.githubviewer.ui.components.dashboard

import hr.josipva.githubviewer.repository.FakeGithubRepository
import hr.josipva.githubviewer.ui.components.dashboard.DashboardViewModel.Intent.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class DashboardViewModelTest {

    private lateinit var dashboardViewModel: DashboardViewModel

    @Before
    fun setUp() {
        dashboardViewModel = DashboardViewModel(
            dispatcher = TestCoroutineDispatcher(),
            githubRepository = FakeGithubRepository()
        )
    }

    @Test
    fun clearSearchResetsStateExceptSort() {
        val initialSortValue = dashboardViewModel.state.sortBy
        val newSortValue = initialSortValue + "test"
        with(dashboardViewModel) {
            send(Search("android"))
            send(Sort(newSortValue))
            send(ClearSearch)
            assertEquals(state.sortBy, newSortValue)
        }
    }
}