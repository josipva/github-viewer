package hr.josipva.githubviewer.repository

import hr.josipva.githubviewer.model.RepositoryItem
import hr.josipva.githubviewer.model.User
import hr.josipva.githubviewer.network.Resource
import io.mockk.mockk

class FakeGithubRepository : GithubRepository {
    override suspend fun searchRepositories(
        query: String,
        sort: String
    ): Resource<List<RepositoryItem>> = mockk()

    override suspend fun getRepository(
        userLogin: String,
        repoName: String
    ): Resource<RepositoryItem> = Resource.Success(mockk())

    override suspend fun getUser(userLogin: String): Resource<User> = Resource.Success(mockk())
}