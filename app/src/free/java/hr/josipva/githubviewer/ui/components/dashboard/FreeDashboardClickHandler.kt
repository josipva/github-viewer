package hr.josipva.githubviewer.ui.components.dashboard

import android.content.Context
import androidx.navigation.NavHostController
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.model.RepositoryItem
import hr.josipva.githubviewer.util.showToast

object FreeDashboardClickHandler : DashboardClickHandler {
    override fun onItemClick(context: Context) = context.showToast(R.string.free_version_warning)

    override fun onItemClick(navHostController: NavHostController, repositoryItem: RepositoryItem) =
        Unit

    override fun onAvatarClick(context: Context, url: String) = Unit

    override fun onAvatarClick(context: Context) = context.showToast(R.string.free_version_warning)
}