package hr.josipva.githubviewer.main

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import coil.annotation.ExperimentalCoilApi
import hr.josipva.githubviewer.ui.Routes.Dashboard
import hr.josipva.githubviewer.ui.components.dashboard.DashboardScreen
import hr.josipva.githubviewer.ui.components.dashboard.FreeDashboardClickHandler

@ExperimentalCoilApi
@Composable
fun MainNavHost(navHostController: NavHostController) = NavHost(
    navController = navHostController,
    startDestination = Dashboard.route
) {
    composable(Dashboard.route) {
        DashboardScreen(
            navHostController = navHostController,
            dashboardViewModel = hiltViewModel(),
            dashboardClickHandler = FreeDashboardClickHandler
        )
    }
}