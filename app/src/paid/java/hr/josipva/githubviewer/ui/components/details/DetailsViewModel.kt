package hr.josipva.githubviewer.ui.components.details

import androidx.annotation.StringRes
import androidx.lifecycle.SavedStateHandle
import dagger.hilt.android.lifecycle.HiltViewModel
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.base.StatefulIntentViewModel
import hr.josipva.githubviewer.model.RepositoryItem
import hr.josipva.githubviewer.model.User
import hr.josipva.githubviewer.network.Resource.*
import hr.josipva.githubviewer.repository.GithubRepository
import hr.josipva.githubviewer.ui.Routes
import hr.josipva.githubviewer.ui.components.details.DetailsViewModel.*
import hr.josipva.githubviewer.ui.components.details.DetailsViewModel.Event.ShowWarning
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    dispatcher: CoroutineDispatcher,
    savedStateHandle: SavedStateHandle,
    private val githubRepository: GithubRepository
) : StatefulIntentViewModel<Intent, Event, State>(dispatcher, State()) {

    sealed class Intent
    sealed class Event {
        data class ShowWarning(@StringRes val message: Int) : Event()
    }

    data class State(
        val isLoading: Boolean = true,
        val repository: RepositoryItem? = null,
        val user: User? = null
    )

    init {
        with(savedStateHandle) {
            val userLogin = requireNotNull(get<String>(Routes.Details.loginArg))
            val repoName = requireNotNull(get<String>(Routes.Details.repoNameArg))
            launch {
                getRepository(userLogin = userLogin, repoName = repoName)
                getUser(userLogin)
                setState { copy(isLoading = false) }
            }
        }
    }

    private suspend fun getRepository(userLogin: String, repoName: String) {
        when (val result =
            githubRepository.getRepository(userLogin = userLogin, repoName = repoName)) {
            is Success -> setState { copy(repository = result.value) }
            is NetworkError -> ShowWarning(R.string.server_unavailable).send()
            is HttpError -> {
                if (result.code == 403) ShowWarning(R.string.forbidden).send()
                else ShowWarning(R.string.something_went_wrong).send()
            }
            else -> ShowWarning(R.string.something_went_wrong).send()
        }
    }

    private suspend fun getUser(userLogin: String) {
        when (val result = githubRepository.getUser(userLogin)) {
            is Success -> setState { copy(user = result.value) }
            is NetworkError -> ShowWarning(R.string.server_unavailable).send()
            else -> ShowWarning(R.string.something_went_wrong).send()
        }
    }

    override suspend fun handleIntent(intent: Intent) = Unit
}