package hr.josipva.githubviewer.ui.components.dashboard

import android.content.Context
import androidx.navigation.NavHostController
import hr.josipva.githubviewer.model.RepositoryItem
import hr.josipva.githubviewer.ui.Routes
import hr.josipva.githubviewer.util.openWebPage

object PaidDashboardClickHandler : DashboardClickHandler {
    override fun onItemClick(context: Context) = Unit

    override fun onItemClick(navHostController: NavHostController, repositoryItem: RepositoryItem) {
        navHostController.navigate(
            Routes.Details.buildFullRoute(
                loginArg = repositoryItem.owner.login,
                repoNameArg = repositoryItem.name
            )
        )
    }

    override fun onAvatarClick(context: Context, url: String) {
        context.openWebPage(url)
    }

    override fun onAvatarClick(context: Context) = Unit
}