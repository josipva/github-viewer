package hr.josipva.githubviewer.ui.components.details

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.model.RepositoryItem
import hr.josipva.githubviewer.model.User

@ExperimentalCoilApi
@Composable
fun DetailsView(
    user: User,
    repository: RepositoryItem,
    onSectionClick: (url: String) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Image(
            painter = rememberImagePainter(
                data = user.avatarUrl,
                builder = { crossfade(true) }),
            contentDescription = null,
            modifier = Modifier
                .size(120.dp)
                .padding(16.dp)
                .clip(CircleShape)
                .clickable { onSectionClick(user.htmlUrl) },
            contentScale = ContentScale.Crop
        )
        Text(
            modifier = Modifier.padding(horizontal = 10.dp),
            text = repository.fullName,
            color = colorResource(id = R.color.primary_color),
            fontWeight = FontWeight.Bold,
            overflow = TextOverflow.Ellipsis,
            maxLines = 1
        )
        RepositorySection(repositoryItem = repository, onClick = onSectionClick)
        OwnerSection(user = user, onClick = onSectionClick)
    }
}