package hr.josipva.githubviewer.ui.components.details

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.model.User

@Composable
fun OwnerSection(user: User, onClick: (url: String) -> Unit) {
    user.name?.let { name ->
        DetailsSection(
            title = stringResource(R.string.owner),
            modifier = Modifier.clickable { onClick(user.htmlUrl) }) {

            val style = TextStyle(fontSize = 12.sp, color = Color.Gray)


            Text(text = name, fontSize = 12.sp)
            user.location?.let { location ->
                Text(
                    text = stringResource(id = R.string.location, location),
                    style = style,
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
            }
            user.email?.let { email ->
                Text(
                    text = stringResource(id = R.string.email, email),
                    style = style,
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
            }
            user.bio?.let { bio ->
                Text(
                    text = bio,
                    style = style,
                    modifier = Modifier.padding(top = 8.dp)
                )
            }
        }
    }
}