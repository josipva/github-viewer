package hr.josipva.githubviewer.ui.components.details

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.ui.components.BackArrow

@Composable
fun DetailsTopBar(
    navHostController: NavHostController,
) {
    Row(
        modifier = Modifier
            .background(colorResource(id = R.color.primary_color))
            .fillMaxWidth()
            .height(54.dp), verticalAlignment = Alignment.CenterVertically
    ) {
        BackArrow(navHostController)
        Text(
            modifier = Modifier.padding(vertical = 4.dp),
            text = stringResource(R.string.details),
            color = MaterialTheme.colors.background,
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold
        )
    }
}
