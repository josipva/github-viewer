package hr.josipva.githubviewer.ui.components.details

import android.content.Context
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import coil.annotation.ExperimentalCoilApi
import hr.josipva.githubviewer.ui.components.details.DetailsViewModel.Event.ShowWarning
import hr.josipva.githubviewer.util.openWebPage
import hr.josipva.githubviewer.util.showToast
import kotlinx.coroutines.flow.collect

@ExperimentalCoilApi
@Composable
fun DetailsScreen(
    navHostController: NavHostController,
    detailsViewModel: DetailsViewModel
) {
    val viewState by detailsViewModel.states.collectAsState()
    val context = LocalContext.current

    detailsViewModel.CollectEvents(context)

    Column {
        DetailsTopBar(navHostController)
        if (viewState.isLoading) {
            Box(modifier = Modifier.fillMaxSize()) {
                CircularProgressIndicator(Modifier.align(Alignment.Center))
            }
        } else {
            viewState.user?.let { user ->
                viewState.repository?.let { repository ->
                    DetailsView(
                        modifier = Modifier.padding(16.dp),
                        user = user,
                        repository = repository,
                        onSectionClick = { url -> context.openWebPage(url) })
                }
            }
        }
    }
}

@Composable
private fun DetailsViewModel.CollectEvents(context: Context) {
    LaunchedEffect(Unit) {
        events.collect { event ->
            when (event) {
                is ShowWarning -> context.showToast(event.message)
            }
        }
    }
}