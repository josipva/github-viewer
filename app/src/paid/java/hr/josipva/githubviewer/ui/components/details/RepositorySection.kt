package hr.josipva.githubviewer.ui.components.details

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.model.RepositoryItem
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@Composable
fun RepositorySection(repositoryItem: RepositoryItem, onClick: (url: String) -> Unit) {
    DetailsSection(
        title = stringResource(R.string.repository),
        modifier = Modifier
            .padding(vertical = 16.dp)
            .clickable { onClick(repositoryItem.htmlUrl) }) {

        val sourceFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")

        val dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)

        val style = TextStyle(fontSize = 12.sp, color = Color.Gray)

        Text(text = repositoryItem.name, fontSize = 12.sp)
        repositoryItem.language?.let { language ->
            Text(
                text = stringResource(id = R.string.language, language),
                style = style,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1
            )
        }
        Text(
            text = stringResource(
                id = R.string.created_at,
                dateTimeFormatter.format(
                    LocalDateTime.parse(
                        repositoryItem.createdAt,
                        sourceFormatter
                    )
                )
            ),
            style = style,
            overflow = TextOverflow.Ellipsis,
            maxLines = 1
        )
        Text(
            text = stringResource(
                id = R.string.updated_at,
                dateTimeFormatter.format(
                    LocalDateTime.parse(
                        repositoryItem.updatedAt,
                        sourceFormatter
                    )
                )
            ),
            style = style,
            overflow = TextOverflow.Ellipsis,
            maxLines = 1
        )
        repositoryItem.description?.let { description ->
            Text(
                text = description,
                style = style,
                modifier = Modifier.padding(top = 8.dp)
            )
        }
    }
}