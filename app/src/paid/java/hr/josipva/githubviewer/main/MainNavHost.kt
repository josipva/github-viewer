package hr.josipva.githubviewer.main

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import coil.annotation.ExperimentalCoilApi
import hr.josipva.githubviewer.ui.Routes.Dashboard
import hr.josipva.githubviewer.ui.Routes.Details
import hr.josipva.githubviewer.ui.components.dashboard.DashboardScreen
import hr.josipva.githubviewer.ui.components.dashboard.PaidDashboardClickHandler
import hr.josipva.githubviewer.ui.components.details.DetailsScreen

@ExperimentalCoilApi
@Composable
fun MainNavHost(navHostController: NavHostController) = NavHost(
    navController = navHostController,
    startDestination = Dashboard.route
) {
    composable(Dashboard.route) {
        DashboardScreen(
            navHostController = navHostController,
            dashboardViewModel = hiltViewModel(),
            dashboardClickHandler = PaidDashboardClickHandler
        )
    }
    composable(
        route = Details.routeWithArgs,
        arguments = listOf(
            navArgument(Details.loginArg) {
                type = NavType.StringType
            },
            navArgument(Details.repoNameArg) {
                type = NavType.StringType
            })
    ) {
        DetailsScreen(
            navHostController = navHostController,
            detailsViewModel = hiltViewModel()
        )
    }
}