package hr.josipva.githubviewer.main

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import coil.annotation.ExperimentalCoilApi
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import hr.josipva.githubviewer.MainActivity
import hr.josipva.githubviewer.ui.components.dashboard.dashboardTag
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoilApi
@HiltAndroidTest
class MainNavHostKtTest {

    @get:Rule(order = 1)
    var hiltTestRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    var composeTestRule = createAndroidComposeRule<MainActivity>()

    lateinit var navHostController: NavHostController

    @Before
    fun setupMainNavHost() {
        composeTestRule.setContent {
            navHostController = rememberNavController()
            MainNavHost(navHostController)
        }
    }

    @Test
    fun dashboardIsDefaultScreen() {
        composeTestRule
            .onNodeWithTag(dashboardTag)
            .assertIsDisplayed()
    }

}