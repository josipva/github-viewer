package hr.josipva.githubviewer.repository

import hr.josipva.githubviewer.model.RepositoryItem
import hr.josipva.githubviewer.model.User
import hr.josipva.githubviewer.network.Resource

interface GithubRepository {
    suspend fun searchRepositories(query: String, sort: String): Resource<List<RepositoryItem>>
    suspend fun getRepository(userLogin: String, repoName: String): Resource<RepositoryItem>
    suspend fun getUser(userLogin: String): Resource<User>
}