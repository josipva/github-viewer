package hr.josipva.githubviewer.repository

import hr.josipva.githubviewer.model.RepositoryItem
import hr.josipva.githubviewer.model.User
import hr.josipva.githubviewer.network.api.GitHubApi
import hr.josipva.githubviewer.network.apiCall
import hr.josipva.githubviewer.network.dto.RepositoriesDto
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class DefaultGithubRepository @Inject constructor(
    private val dispatcher: CoroutineDispatcher,
    private val gitHubApi: GitHubApi
) : GithubRepository {

    override suspend fun searchRepositories(query: String, sort: String) = apiCall(dispatcher) {
        return@apiCall gitHubApi.searchRepositories(query = query, sort = sort).mapToDomain()
    }

    override suspend fun getRepository(userLogin: String, repoName: String) = apiCall(dispatcher) {
        return@apiCall RepositoryItem.fromDto(
            gitHubApi.getRepository(
                userLogin = userLogin,
                repoName = repoName
            )
        )
    }

    override suspend fun getUser(userLogin: String) = apiCall(dispatcher) {
        return@apiCall User.fromDto(
            gitHubApi.getUser(userLogin = userLogin)
        )
    }

    private fun RepositoriesDto.mapToDomain(): List<RepositoryItem> =
        items.map { RepositoryItem.fromDto(it) }
}