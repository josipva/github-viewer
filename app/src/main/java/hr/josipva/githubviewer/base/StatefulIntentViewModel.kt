package hr.josipva.githubviewer.base

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

abstract class StatefulIntentViewModel<Intent, Event, State>(
    dispatcher: CoroutineDispatcher,
    private val initialState: State
) : IntentViewModel<Intent>(dispatcher) {

    private val _events = Channel<Event>()

    val events: Flow<Event>
        get() = _events.consumeAsFlow()

    private var _state = initialState

    val state: State
        get() = _state

    private val _states = MutableStateFlow(initialState)

    val states: StateFlow<State>
        get() = _states.asStateFlow()

    private val stateMutex = Mutex()

    final override fun onCleared() {
        coroutineContext.cancel()
    }

    @JvmName("sendEvent")
    fun Event.send() = launch { _events.send(this@send) }

    protected suspend fun setState(reducer: State.() -> State) = stateMutex.withLock {
        _state = _state.reducer()
        _states.emit(_state)
    }

    protected suspend fun resetState() {
        setState { initialState }
    }
}