package hr.josipva.githubviewer.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

abstract class IntentViewModel<Intent>(dispatcher: CoroutineDispatcher) : ViewModel(),
    CoroutineScope {

    override val coroutineContext: CoroutineContext = dispatcher + Job()

    private val intents = Channel<Intent>()

    protected abstract suspend fun handleIntent(intent: Intent)

    override fun onCleared() {
        coroutineContext.cancel()
    }

    fun send(intent: Intent) = launch {
        intents.send(intent)
    }

    operator fun plusAssign(intent: Intent) {
        send(intent)
    }

    inline fun <T> Flow<T>.launchAndCollect(
        context: CoroutineContext = EmptyCoroutineContext,
        crossinline action: suspend (T) -> Unit
    ) = launch(context) { this@launchAndCollect.collect(action) }

    init {
        launch {
            intents.consumeEach { intent ->
                handleIntent(intent)
            }
        }
    }
}