package hr.josipva.githubviewer.model

import hr.josipva.githubviewer.network.dto.UserDto

data class User(
    val avatarUrl: String,
    val bio: String?,
    val blog: String,
    val email: String?,
    val htmlUrl: String,
    val location: String?,
    val login: String,
    val name: String?,
) {
    companion object {
        fun fromDto(userDto: UserDto) = User(
            avatarUrl = userDto.avatarUrl,
            bio = userDto.bio,
            blog = userDto.blog,
            email = userDto.email,
            htmlUrl = userDto.htmlUrl,
            location = userDto.location,
            login = userDto.login,
            name = userDto.name
        )
    }
}
