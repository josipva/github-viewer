package hr.josipva.githubviewer.model

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Mediation
import androidx.compose.material.icons.sharp.Star
import androidx.compose.material.icons.sharp.Update
import androidx.compose.ui.graphics.vector.ImageVector

enum class RepositorySortType {
    Stars,
    Forks,
    Updated;

    val queryName: String
        get() = this.name.lowercase()

    val icon: ImageVector
        get() = when (this) {
            Stars -> Icons.Sharp.Star
            Forks -> Icons.Sharp.Mediation
            Updated -> Icons.Sharp.Update
        }

    companion object {
        fun getTypeFromQueryName(queryName: String): RepositorySortType =
            when (queryName) {
                Stars.queryName -> Stars
                Forks.queryName -> Forks
                Updated.queryName -> Updated
                else -> throw (Throwable("non existing RepositorySortType"))
            }
    }
}