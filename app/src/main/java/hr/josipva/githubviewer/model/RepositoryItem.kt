package hr.josipva.githubviewer.model

import hr.josipva.githubviewer.network.dto.RepositoryItemDto

data class RepositoryItem(
    val name: String,
    val owner: Owner,
    val description: String?,
    val watchers: Int,
    val forks: Int,
    val openIssues: Int,
    val fullName: String,
    val createdAt: String,
    val updatedAt: String,
    val htmlUrl: String,
    val language: String?
) {
    companion object {
        fun fromDto(repositoryItemDto: RepositoryItemDto) = with(repositoryItemDto) {
            RepositoryItem(
                name = name,
                owner = Owner.fromDto(owner),
                description = description,
                watchers = watchers,
                forks = forks,
                openIssues = openIssues,
                fullName = fullName,
                createdAt = createdAt,
                updatedAt = updatedAt,
                htmlUrl = htmlUrl,
                language = language
            )
        }
    }
}
