package hr.josipva.githubviewer.model

import hr.josipva.githubviewer.network.dto.OwnerDto

data class Owner(
    val login: String,
    val avatarUrl: String,
    val htmlUrl: String
) {
    companion object {
        fun fromDto(ownerDto: OwnerDto) = Owner(
            login = ownerDto.login,
            avatarUrl = ownerDto.avatarUrl,
            htmlUrl = ownerDto.htmlUrl
        )
    }
}