package hr.josipva.githubviewer.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hr.josipva.githubviewer.BuildConfig
import hr.josipva.githubviewer.network.api.GitHubApi
import hr.josipva.githubviewer.util.gitHubBaseUrl
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    fun providesOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(Interceptor { interceptorChain ->
            val originalRequest = interceptorChain.request()
            // Explicitly request V3 via the Accept header.
            val newRequest = originalRequest.newBuilder()
                .addHeader("Accept", "application/vnd.github.v3+json")
                .method(originalRequest.method, originalRequest.body)
                .build()
            return@Interceptor interceptorChain.proceed(newRequest)
        })
        .addInterceptor(
            HttpLoggingInterceptor().setLevel(
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                else HttpLoggingInterceptor.Level.NONE
            )
        ).build()

    @ExperimentalSerializationApi
    @Provides
    fun providesRetrofitBuilder(okHttpClient: OkHttpClient): Retrofit.Builder {
        val contentType = "application/json".toMediaType()
        val json = Json {
            isLenient = true
            ignoreUnknownKeys = true
        }
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(json.asConverterFactory(contentType))
    }

    @Provides
    @Singleton
    fun providesGitHubApi(retrofitBuilder: Retrofit.Builder): GitHubApi =
        retrofitBuilder.baseUrl(gitHubBaseUrl).build().create(GitHubApi::class.java)

}