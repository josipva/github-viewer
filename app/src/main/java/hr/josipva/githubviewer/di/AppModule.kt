package hr.josipva.githubviewer.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hr.josipva.githubviewer.network.api.GitHubApi
import hr.josipva.githubviewer.repository.DefaultGithubRepository
import hr.josipva.githubviewer.repository.GithubRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun providesMainCoroutineDispatcher(): CoroutineDispatcher = Dispatchers.Main

    @Provides
    @Singleton
    fun providesGithubRepository(gitHubApi: GitHubApi): GithubRepository =
        DefaultGithubRepository(Dispatchers.IO, gitHubApi)
}