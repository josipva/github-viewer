package hr.josipva.githubviewer

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GitHubViewerApplication : Application()