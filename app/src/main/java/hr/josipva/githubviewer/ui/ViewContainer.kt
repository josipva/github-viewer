package hr.josipva.githubviewer.ui

import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import hr.josipva.githubviewer.ui.theme.GitHubViewerTheme

@Composable
fun ViewContainer(content: @Composable () -> Unit) {
    GitHubViewerTheme {
        Scaffold {
            content()
        }
    }
}