package hr.josipva.githubviewer.ui.components.dashboard.topbar

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Mediation
import androidx.compose.material.icons.sharp.Star
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

data class TopBarDropdownMenuEntry(
    val icon: ImageVector,
    val text: String,
    val queryValue: String
)

@Composable
fun TopBarDropdownMenu(
    modifier: Modifier = Modifier,
    selectedEntry: TopBarDropdownMenuEntry,
    entries: List<TopBarDropdownMenuEntry>,
    onEntrySelected: (String) -> Unit
) {
    var expanded by remember { mutableStateOf(false) }

    Column(modifier) {
        IconButton(
            content = {
                Icon(
                    imageVector = selectedEntry.icon,
                    contentDescription = "sortButton",
                    modifier = Modifier.size(24.dp),
                    tint = MaterialTheme.colors.background
                )
            },
            onClick = { expanded = !expanded })
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
        ) {
            entries.forEach { entry ->
                DropdownMenuItem(
                    content = {
                        Text(text = entry.text)
                    },
                    onClick = {
                        onEntrySelected(entry.queryValue)
                        expanded = false
                    },
                )
            }
        }
    }
}

@Preview
@Composable
private fun Preview() {
    TopBarDropdownMenu(
        selectedEntry = TopBarDropdownMenuEntry(
            icon = Icons.Sharp.Star,
            text = "By Stars",
            queryValue = "stars"
        ),
        entries =
        listOf(
            TopBarDropdownMenuEntry(
                icon = Icons.Sharp.Star,
                text = "By Stars",
                queryValue = "stars"
            ),
            TopBarDropdownMenuEntry(
                icon = Icons.Sharp.Mediation,
                text = "By Forks",
                queryValue = "forks"
            )
        ),
        onEntrySelected = {})
}