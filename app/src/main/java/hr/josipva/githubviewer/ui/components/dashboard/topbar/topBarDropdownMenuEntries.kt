package hr.josipva.githubviewer.ui.components.dashboard.topbar

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.model.RepositorySortType

@Composable
fun topBarDropdownMenuEntries() = listOf(
    TopBarDropdownMenuEntry(
        icon = RepositorySortType.Stars.icon,
        text = stringResource(
            R.string.sort_by,
            RepositorySortType.Stars.queryName
        ),
        queryValue = RepositorySortType.Stars.queryName
    ),
    TopBarDropdownMenuEntry(
        icon = RepositorySortType.Forks.icon,
        text = stringResource(
            R.string.sort_by,
            RepositorySortType.Forks.queryName
        ),
        queryValue = RepositorySortType.Forks.queryName
    ),
    TopBarDropdownMenuEntry(
        icon = RepositorySortType.Updated.icon,
        text = stringResource(
            R.string.sort_by,
            RepositorySortType.Updated.queryName
        ),
        queryValue = RepositorySortType.Updated.queryName
    )
)