package hr.josipva.githubviewer.ui.components.dashboard

import android.content.Context
import androidx.navigation.NavHostController
import hr.josipva.githubviewer.model.RepositoryItem

interface DashboardClickHandler {
    fun onItemClick(context: Context)
    fun onItemClick(navHostController: NavHostController, repositoryItem: RepositoryItem)
    fun onAvatarClick(context: Context, url: String)
    fun onAvatarClick(context: Context)
}