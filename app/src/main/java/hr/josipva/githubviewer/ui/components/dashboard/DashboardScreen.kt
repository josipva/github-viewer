package hr.josipva.githubviewer.ui.components.dashboard

import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import coil.annotation.ExperimentalCoilApi
import com.airbnb.lottie.LottieComposition
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.ui.components.dashboard.DashboardViewModel.Event.ShowWarning
import hr.josipva.githubviewer.ui.components.dashboard.DashboardViewModel.Intent.*
import hr.josipva.githubviewer.ui.components.dashboard.topbar.DashboardTopBar
import hr.josipva.githubviewer.util.showToast
import kotlinx.coroutines.flow.collect

const val dashboardTag: String = "dashboard"

@ExperimentalCoilApi
@Composable
fun DashboardScreen(
    navHostController: NavHostController,
    dashboardViewModel: DashboardViewModel,
    dashboardClickHandler: DashboardClickHandler
) {
    val viewState by dashboardViewModel.states.collectAsState()
    val context = LocalContext.current

    dashboardViewModel.CollectEvents(context)

    Column(Modifier.testTag(dashboardTag)) {
        DashboardTopBar(
            searchActive = viewState.searchQuery.isNotBlank(),
            onSearchCleared = { dashboardViewModel += ClearSearch },
            onQuery = { query -> dashboardViewModel += Search(query) },
            value = viewState.searchQuery,
            selectedSortEntry = viewState.sortBy,
            onSortEntrySelected = { sortValue -> dashboardViewModel += Sort(sortValue) }
        )

        if (viewState.isLoading) {
            Box(modifier = Modifier.fillMaxSize()) {
                CircularProgressIndicator(Modifier.align(Alignment.Center))
            }
        } else {
            if (viewState.repositories.isEmpty()) {
                Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                    if (viewState.isInitialSearch) {
                        GithubLogoAnimation()
                    } else {
                        NothingFoundAnimation()
                    }
                }
            } else {
                RepositoryList(
                    repositories = viewState.repositories,
                    onItemClick = { repositoryItem ->
                        // Free & Paid versions
                        dashboardClickHandler.onItemClick(context)
                        dashboardClickHandler.onItemClick(navHostController, repositoryItem)
                    },
                    onAvatarClick = { url ->
                        dashboardClickHandler.onAvatarClick(context)
                        dashboardClickHandler.onAvatarClick(context, url)
                    })
            }
        }
    }
}

@Composable
private fun DashboardViewModel.CollectEvents(context: Context) {
    LaunchedEffect(Unit) {
        events.collect { event ->
            when (event) {
                is ShowWarning -> context.showToast(event.message)
            }
        }
    }
}

@Composable
private fun GithubLogoAnimation() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.github_octocat))
    DashboardLottieAnimation(composition)
}

@Composable
private fun NothingFoundAnimation() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.nothing_found))
    Column {
        DashboardLottieAnimation(composition)
        Text(
            stringResource(R.string.nothing_found),
            modifier = Modifier.padding(8.dp),
            color = Color.Gray
        )
    }
}

@Composable
private fun DashboardLottieAnimation(composition: LottieComposition?) {
    LottieAnimation(
        composition = composition,
        iterations = LottieConstants.IterateForever,
        modifier = Modifier.size(120.dp)
    )
}