package hr.josipva.githubviewer.ui.components.dashboard

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.BugReport
import androidx.compose.material.icons.sharp.Mediation
import androidx.compose.material.icons.sharp.Star
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.model.RepositoryItem

@ExperimentalCoilApi
@Composable
fun RepositoryItemView(
    modifier: Modifier = Modifier,
    item: RepositoryItem,
    onItemClick: (repositoryItem: RepositoryItem) -> Unit,
    onAvatarClick: (url: String) -> Unit
) {
    Card(
        elevation = 4.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Column(modifier = modifier
            .fillMaxWidth()
            .clickable { onItemClick(item) }) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Image(
                    painter = rememberImagePainter(
                        data = item.owner.avatarUrl,
                        builder = { crossfade(true) }),
                    contentDescription = null,
                    modifier = Modifier
                        .size(32.dp)
                        .clip(CircleShape)
                        .clickable { onAvatarClick(item.owner.htmlUrl) },
                    contentScale = ContentScale.Crop
                )
                Text(
                    modifier = Modifier.padding(horizontal = 10.dp),
                    text = item.fullName,
                    color = colorResource(id = R.color.primary_color),
                    fontWeight = FontWeight.Bold,
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
            }
            item.description?.let {
                Text(
                    modifier = Modifier.padding(vertical = 8.dp),
                    text = it,
                    fontSize = 12.sp,
                    color = Color.Gray,
                    maxLines = 4,
                    overflow = TextOverflow.Ellipsis
                )
            }
            Row(
                modifier = Modifier.padding(top = 16.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(3.dp)
            ) {
                Icon(
                    modifier = Modifier.size(12.dp),
                    imageVector = Icons.Sharp.Star,
                    contentDescription = "stars",
                    tint = Color.Gray
                )
                Text(text = item.watchers.toString(), fontSize = 10.sp, color = Color.Gray)
                Spacer(modifier = Modifier.width(16.dp))
                Icon(
                    modifier = Modifier.size(12.dp),
                    imageVector = Icons.Sharp.Mediation,
                    contentDescription = "stars",
                    tint = Color.Gray
                )
                Text(text = item.forks.toString(), fontSize = 10.sp, color = Color.Gray)
                Spacer(modifier = Modifier.width(16.dp))
                Icon(
                    modifier = Modifier.size(12.dp),
                    imageVector = Icons.Sharp.BugReport,
                    contentDescription = "issues",
                    tint = Color.Gray
                )
                Text(text = item.openIssues.toString(), fontSize = 10.sp, color = Color.Gray)
            }
        }
    }
}