package hr.josipva.githubviewer.ui.components

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController

@Composable
fun BackArrow(
    navHostController: NavHostController,
    modifier: Modifier = Modifier
) {
    IconButton(
        modifier = modifier,
        onClick = navHostController::popBackStack,
        content = {
            Icon(
                imageVector = Icons.Rounded.ArrowBack,
                contentDescription = "back",
                tint = MaterialTheme.colors.background
            )
        }
    )
}