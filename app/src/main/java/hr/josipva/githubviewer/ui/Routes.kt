package hr.josipva.githubviewer.ui

sealed class Routes(
    val route: String
) {
    object Dashboard : Routes(route = "dashboard")
    object Details : Routes(route = "details") {
        const val routeWithArgs: String = "details/{login}/{repoName}"
        const val loginArg: String = "login"
        const val repoNameArg: String = "repoName"
        fun buildFullRoute(loginArg: String, repoNameArg: String) = "$route/$loginArg/$repoNameArg"
    }
}
