package hr.josipva.githubviewer.ui.components.dashboard.topbar

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Close
import androidx.compose.material.icons.sharp.Search
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.model.RepositorySortType

@Composable
fun DashboardTopBar(
    modifier: Modifier = Modifier,
    searchActive: Boolean,
    onSearchCleared: () -> Unit,
    onQuery: (String) -> Unit,
    selectedSortEntry: String,
    onSortEntrySelected: (String) -> Unit,
    value: String
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .background(colorResource(id = R.color.primary_color))
            .padding(8.dp)
    ) {
        val focusManager = LocalFocusManager.current

        Row(horizontalArrangement = Arrangement.SpaceBetween, modifier = Modifier.fillMaxWidth()) {
            Text(
                modifier = Modifier.padding(vertical = 4.dp),
                text = stringResource(R.string.app_name),
                color = MaterialTheme.colors.background,
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold
            )

        }

        ConstraintLayout(modifier = Modifier.fillMaxWidth()) {
            val (searchField, sortButton) = createRefs()
            TextField(
                modifier = Modifier
                    .padding(vertical = 4.dp)
                    .constrainAs(searchField) {
                        width = Dimension.fillToConstraints
                        start.linkTo(parent.start)
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        end.linkTo(sortButton.start)
                    },
                value = value,
                placeholder = { Text(stringResource(R.string.search_repositories)) },
                onValueChange = {
                    onQuery(it)
                },
                keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
                singleLine = true,
                shape = RoundedCornerShape(percent = 8),
                colors = TextFieldDefaults.textFieldColors(
                    cursorColor = colorResource(id = R.color.primary_color),
                    backgroundColor = MaterialTheme.colors.background,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent
                ),
                leadingIcon = {
                    Icon(
                        imageVector = Icons.Sharp.Search,
                        contentDescription = "search"
                    )
                },
                trailingIcon = {
                    if (searchActive) {
                        Icon(
                            imageVector = Icons.Sharp.Close,
                            contentDescription = "clearSearch",
                            modifier = Modifier.clickable { onSearchCleared() })
                    }
                }
            )
            TopBarDropdownMenu(
                modifier = Modifier
                    .constrainAs(sortButton) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        end.linkTo(parent.end)
                    }
                    .padding(start = 8.dp),
                selectedEntry = TopBarDropdownMenuEntry(
                    icon = RepositorySortType.getTypeFromQueryName(selectedSortEntry).icon,
                    text = stringResource(
                        R.string.sort_by,
                        selectedSortEntry
                    ),
                    queryValue = selectedSortEntry
                ),
                entries = topBarDropdownMenuEntries(),
                onEntrySelected = { entry -> onSortEntrySelected(entry) }
            )
        }
    }
}

@Composable
@Preview
private fun Preview() {
    DashboardTopBar(
        onQuery = {},
        onSearchCleared = {},
        searchActive = false,
        value = "Search",
        selectedSortEntry = RepositorySortType.Stars.queryName,
        onSortEntrySelected = {}
    )
}