package hr.josipva.githubviewer.ui.components.dashboard

import androidx.annotation.StringRes
import dagger.hilt.android.lifecycle.HiltViewModel
import hr.josipva.githubviewer.R
import hr.josipva.githubviewer.base.StatefulIntentViewModel
import hr.josipva.githubviewer.model.RepositoryItem
import hr.josipva.githubviewer.model.RepositorySortType
import hr.josipva.githubviewer.network.Resource.NetworkError
import hr.josipva.githubviewer.network.Resource.Success
import hr.josipva.githubviewer.repository.GithubRepository
import hr.josipva.githubviewer.ui.components.dashboard.DashboardViewModel.*
import hr.josipva.githubviewer.ui.components.dashboard.DashboardViewModel.Event.ShowWarning
import hr.josipva.githubviewer.ui.components.dashboard.DashboardViewModel.Intent.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
    dispatcher: CoroutineDispatcher,
    private val githubRepository: GithubRepository
) : StatefulIntentViewModel<Intent, Event, State>(dispatcher, State()) {

    private var searchJob: Job? = null

    sealed class Intent {
        data class Search(val query: String) : Intent()
        object ClearSearch : Intent()
        data class Sort(val repositorySortType: String) : Intent()
    }

    sealed class Event {
        data class ShowWarning(@StringRes val message: Int) : Event()
    }

    data class State(
        val isLoading: Boolean = false,
        val isInitialSearch: Boolean = true,
        val searchQuery: String = String(),
        val repositories: List<RepositoryItem> = emptyList(),
        val sortBy: String = RepositorySortType.Stars.queryName
    )

    override suspend fun handleIntent(intent: Intent) = when (intent) {
        is Search -> intent.handle()
        is ClearSearch -> clearStateExceptSort()
        is Sort -> intent.handle()
    }

    private suspend fun Search.handle() {
        setState { copy(searchQuery = query) }
        debouncedSearch()
    }

    private suspend fun Sort.handle() {
        setState { copy(sortBy = repositorySortType) }
        debouncedSearch()
    }

    private suspend fun debouncedSearch() {
        searchJob?.cancel()
        if (state.searchQuery.isBlank()) return

        searchJob = launch {
            delay(500)
            setState { copy(isLoading = true) }
            val result = githubRepository.searchRepositories(
                query = state.searchQuery,
                sort = state.sortBy
            )
            when (result) {
                is Success -> {
                    setState {
                        copy(
                            isLoading = false,
                            repositories = result.value,
                            isInitialSearch = false
                        )
                    }
                }
                is NetworkError -> {
                    clearNonInputState()
                    ShowWarning(R.string.server_unavailable).send()
                }
                else -> {
                    clearNonInputState()
                    ShowWarning(R.string.something_went_wrong).send()
                }
            }
        }
    }

    private suspend fun clearStateExceptSort() {
        setState { State(sortBy = state.sortBy) }
    }

    private suspend fun clearNonInputState() {
        setState {
            copy(
                isLoading = false,
                isInitialSearch = false,
                repositories = emptyList()
            )
        }
    }
}