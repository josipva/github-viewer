package hr.josipva.githubviewer.ui.components.dashboard

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.annotation.ExperimentalCoilApi
import hr.josipva.githubviewer.model.RepositoryItem

@ExperimentalCoilApi
@Composable
fun RepositoryList(
    modifier: Modifier = Modifier,
    repositories: List<RepositoryItem>,
    onItemClick: (repositoryItem: RepositoryItem) -> Unit,
    onAvatarClick: (url: String) -> Unit
) {
    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        items(repositories) { repositoryItem ->
            RepositoryItemView(
                modifier = Modifier.padding(12.dp),
                item = repositoryItem,
                onItemClick = onItemClick,
                onAvatarClick = onAvatarClick
            )
        }
    }
}