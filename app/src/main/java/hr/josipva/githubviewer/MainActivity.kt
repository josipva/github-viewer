package hr.josipva.githubviewer

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.rememberNavController
import coil.annotation.ExperimentalCoilApi
import dagger.hilt.android.AndroidEntryPoint
import hr.josipva.githubviewer.main.MainNavHost
import hr.josipva.githubviewer.ui.ViewContainer

@ExperimentalCoilApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ViewContainer {
                val navHostController = rememberNavController()
                MainNavHost(navHostController)
            }
        }
    }
}