package hr.josipva.githubviewer.network

import okhttp3.Response

sealed class Resource<out T> {
    data class Success<out T>(val value: T) : Resource<T>()
    data class HttpError(val code: Int? = null, val error: Response? = null) : Resource<Nothing>()
    data class GenericError(val message: String?, val stackTrace: String) : Resource<Nothing>()
    object NetworkError : Resource<Nothing>()
}