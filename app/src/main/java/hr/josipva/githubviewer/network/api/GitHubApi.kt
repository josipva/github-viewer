package hr.josipva.githubviewer.network.api

import hr.josipva.githubviewer.network.dto.RepositoriesDto
import hr.josipva.githubviewer.network.dto.RepositoryItemDto
import hr.josipva.githubviewer.network.dto.UserDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitHubApi {
    @GET("search/repositories")
    suspend fun searchRepositories(
        @Query("q") query: String,
        @Query("sort") sort: String
    ): RepositoriesDto

    @GET("repos/{userLogin}/{repoName}")
    suspend fun getRepository(
        @Path("userLogin") userLogin: String,
        @Path("repoName") repoName: String
    ): RepositoryItemDto

    @GET("users/{userLogin}")
    suspend fun getUser(
        @Path("userLogin") userLogin: String
    ): UserDto
}