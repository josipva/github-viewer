package hr.josipva.githubviewer.network

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okhttp3.Response
import okio.IOException
import retrofit2.HttpException

suspend fun <T> apiCall(
    dispatcher: CoroutineDispatcher,
    apiCall: suspend () -> T
): Resource<T> {
    return withContext(dispatcher) {
        try {
            Resource.Success(apiCall.invoke())
        } catch (throwable: Throwable) {
            when (throwable) {
                is IOException -> Resource.NetworkError
                is HttpException -> {
                    val code = throwable.code()
                    val errorResponse = convertErrorBody(throwable)
                    Resource.HttpError(code, errorResponse)
                }
                else -> {
                    Resource.GenericError(throwable.message, throwable.stackTraceToString())
                }
            }
        }
    }
}

private fun convertErrorBody(throwable: HttpException): Response? {
    return try {
        throwable.response()?.errorBody()?.string()?.let {
            Json.decodeFromString<Response>(it)
        }
    } catch (exception: Exception) {
        null
    }
}