package hr.josipva.githubviewer.network.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RepositoriesDto(
    @SerialName("incomplete_results")
    val incompleteResults: Boolean,
    val items: List<RepositoryItemDto>,
    @SerialName("total_count")
    val totalCount: Int
)
