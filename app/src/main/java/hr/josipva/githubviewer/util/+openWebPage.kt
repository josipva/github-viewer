package hr.josipva.githubviewer.util

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import hr.josipva.githubviewer.R

fun Context.openWebPage(url: String) {
    val webpage: Uri = Uri.parse(url)
    val intent = Intent(Intent.ACTION_VIEW, webpage)
    try {
        startActivity(intent)
    } catch (ex: ActivityNotFoundException) {
        showToast(R.string.web_browser_not_found)
    }
}